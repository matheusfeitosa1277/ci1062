
public class S8P1
{
    private static int SIZE = 200;

    public static void main(String args[])
    {
        int i;
        double valor;
        Imovel moradias[] = new Imovel[SIZE];


        for (i = 0; i < SIZE; i++)
        {
            System.out.printf("%d \n", i);
            if ((i & 1) == 0)
                moradias[i] = new ImovelNovo();
            else moradias[i] = new ImovelVelho();
        }

        for (i = 0; i < SIZE; i++)
        {
            valor =  moradias[i].calcularValorImovel();
            System.out.println(valor);
        }
    }
}

abstract class Imovel 
{
    protected String endereco;
    protected double preco;

    public abstract double calcularValorImovel();
}

class ImovelNovo extends Imovel
{
    public ImovelNovo(){};

    public ImovelNovo(String endereco, double preco)
    {
       this.endereco = endereco; 
       this.preco = preco;
    }

    public double calcularValorImovel()
    {
        return 42;
    }
}

class ImovelVelho extends Imovel
{

    public ImovelVelho(){}
    public ImovelVelho(String endereco, double preco)
    {
       this.endereco = endereco; 
       this.preco = preco;
    }


    public double calcularValorImovel()
    {
        return 0;
    }
}
