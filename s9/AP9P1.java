//1 -  Utilizando interfaces (Java), você pode especificar comportamentos semelhantes
//  para classes possivelmente não relacionadas. Com base nessa ideia, siga as
//  instruções abaixo:
//
//  a. Crie duas classes não relacionadas por herança: classe Produto
//  Eletrônico e a classe Serviço. Dê a cada classe atributos e
//  comportamentos únicos que não estejam relacionados com os
//  atributos e métodos da outra classe. Sugestão:
//      i. Produto Eletrônico – atributo: nome do fabricante, peso,
//      etc.
//      ii. Serviço – formato (remoto, em loja ou em domicílio),
//      duração (em minutos).
//
//  b. Escreva uma interface Loja com os métodos vender e acionar
//  garantia. Faça cada uma das classes Produto Eletrônico e
//  Serviço implementar esta interface. Os métodos em cada classe
//  devem simplesmente apresentar mensagens informativas na tela. Ex.:
//  “Vendendo um serviço!”.
//
//  c. Escreva um aplicativo que crie um objeto de cada uma das duas
//  classes. Crie um objeto ArrayList<Loja> e insira as referências dos
//  objetos instanciados nesta coleção. Finalmente, itere pela coleção,
//  chamando polimorficamente o método vender e acionar garantia de
//  cada objeto.

import java.util.ArrayList;
import  java.util.Iterator;

public class AP9P1
{
    public static void main(String args[])
    {
        ProdutoEletronico produto = new ProdutoEletronico();
        Servico servico = new Servico();

        ArrayList<Loja> loja = new ArrayList<Loja>();
        loja.add(produto); loja.add(servico);

        Iterator<Loja> i = loja.iterator();
        while (i.hasNext())
        {
            Loja proximo = i.next();
            proximo.vender();
            proximo.acionarGarantia();
        }
    }
}

interface Loja
{
    public abstract void vender();
    public abstract void acionarGarantia();
}

class ProdutoEletronico implements Loja
{
    private String nomeDoFabricante;
    private double peso;

    public String getNomeDoFabricante() { return this.nomeDoFabricante; }
    public double getPeso() { return this.peso; }

    public void vender()
    {
        System.out.println("Vendendo um Produto Eletronico");
    }

    public void acionarGarantia()
    {
        System.out.println("Garantia do Produto Eletronico");
    }

}

class Servico implements Loja
{
    private String formato;
    private int minutos;

    public String getFormato() { return this.formato; }
    public int getMinutos() { return this.minutos; }


    public void vender()
    {
        System.out.println("Vendendo um Servico");
    }

    public void acionarGarantia()
    {
        System.out.println("Garantia do Servico");
    }

}

