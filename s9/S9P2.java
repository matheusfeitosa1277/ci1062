//2 - Modifique o código do exercício 1, tornando Produto Eletrônico uma classe
//  abstrata, e implementando duas novas subclasses concretas Televisao e
//  Celular.
//
//  a. O aplicativo que cria a coleção de objetos vai continuar funcionando após a
//  modificação na estrutura das classes?
//
//  b. Modifique o aplicativo para que passe a instanciar diretamente objetos
//  Televisao e Celular, incluindo-os na coleção.

// Incompleto ----------------------------------------------

import java.util.ArrayList;
import  java.util.Iterator;

public class S9P2
{
    public static void main(String args[])
    {
        Televisao televisao = new Televisao();
        Celular celular = new Celular();

        Servico servico = new Servico();

        ArrayList<Loja> loja = new ArrayList<Loja>();
        loja.add(televisao); loja.add(celular); loja.add(servico);

        Iterator<Loja> i = loja.iterator();
        while (i.hasNext())
        {
            Loja proximo = i.next();
            proximo.vender();
            proximo.acionarGarantia();
        }
    }
}

interface Loja
{
    public abstract void vender();
    public abstract void acionarGarantia();
}

abstract class ProdutoEletronico
{
    private String nomeDoFabricante;
    private double peso;

    public String getNomeDoFabricante() { return this.nomeDoFabricante; }
    public double getPeso() { return this.peso; }

    public abstract void vender();

    public abstract void acionarGarantia();

}

class Televisao
{
    public void vender()
    {
        System.out.println("Vendendo uma Televisao");
    }

    public void acionarGarantia()
    {
        System.out.println("Garantia de uma Televisao");
    }
}

class Celular 
{
    public void vender()
    {
        System.out.println("Vendendo um Celular");
    }

    public void acionarGarantia()
    {
        System.out.println("Garantia de um Celular");
    }
}

class Servico implements Loja
{
    private String formato;
    private int minutos;

    public String getFormato() { return this.formato; }
    public int getMinutos() { return this.minutos; }


    public void vender()
    {
        System.out.println("Vendendo um Servico");
    }

    public void acionarGarantia()
    {
        System.out.println("Garantia do Servico");
    }

}








