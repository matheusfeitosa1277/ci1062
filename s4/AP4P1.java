//  1) Implemente as seguintes classes usando o conceito de herança:
//      a. Funcionário com os atributos nome, matrícula, se é estrangeiro e métodos
//      get/set.
//      b. Funcionário mensalista com o atributo salário mensal e métodos get/set.
//      c. Funcionário horista com os atributos valor da hora e número de horas
//      trabalhadas e métodos get/set.
//  
//  2) Crie o construtor completo para cada uma das classes do item 1. Dica: use a
//  palavra-chave super.
//
//  -> 3) Crie dois outros construtores, diferente do construtor completo, para as classes
//  funcionário mensalista e funcionário horista. Dica: use a palavra-chave this.
//
//  4) Crie o método imprimir para cada uma das classes do item 1 de forma que todos
//  os atributos da classe sejam impressos na tela. Dica: aplique o conceito de
//  reusabilidade de código.
//
//  5) Utilizando o conceito de sobreposição, crie um método calcular salário final para
//  cada uma das classes do item 1, em que:
//      a. Na classe funcionário o método deve retornar 0.0;
//      b. Na classe funcionário mensalista o método deve retornar o salário
//      mensal;
//      c. Na classe funcionário horista o método deve retornar o resultado do
//      produto de horas trabalhadas e valor da hora.
//
//  6) Crie uma subclasse para a classe funcionário mensalista com um atributo e
//  método get/set.
//
//  7) Crie um método final para a classe funcionário mensalista que não poderá ser
//  herdado pela classe que você criou no item 6. Dica: o método criado deve fazer
//  sentido para o contexto do problema.
//
//  8) Crie um programa principal que instancie um objeto para a classe funcionário
//  horista e mensalista e chame os métodos imprimir e calcular salário final. Dica:
//  após chamar o método calcular salário final imprima o resultado na tela.
//
//  9) Altere o programa principal anterior (item 8) para que seja criado um vetor de
//  objetos de tamanho 100 para cada uma das classes funcionário mensalista e
//  funcionário horista. Em seguida, imprima a média salarial para cada vetor de
//  funcionário (mensalista e horista)

public class AP4P1
{
    public static void main(String args[])
    {
        int i;
    }
    
}

class Funcionario
{
    /* Atributos */
    protected String nome, matricula;
    protected boolean estrangeiro;

    /* Getters and Setter */
    public String getNome() { return this.nome; }

    public String getMatricula() { return this.matricula; }

    public boolean isEstrangeiro() { return this.estrangeiro; }


    public void setNome(String nome) { this.nome = nome; }

    public void setMatricula(String matricula) { this.matricula = matricula; }

    public void  setEstrangeiro(boolean estrangeiro) { this.estrangeiro = estrangeiro; }

    /* Construtores */
    public Funcionario(String nome, String matricula, boolean estrangeiro)
    {
        this.setNome(nome);
        this.setMatricula(matricula);
        this.setEstrangeiro(estrangeiro);
    }


    /* Outros metodos */
}

class Mensalista extends Funcionario
{
   private double salarioMensal; 

   public double getSalarioMensal() { return this.salarioMensal; }

   public void setSalarioMensal(double salario) { this.salarioMensal = salario; }


   public Mensalista(String nome, String matricula, boolean estrangeiro, double salario)
   {
       super(nome, matricula, estrangeiro);
       this.setSalarioMensal(salario);
   }

}

class Horista extends Funcionario
{
    private int numHoras;
    private double valorHora;


    public int getNumHoras() { return this.numHoras; }

    public double getValorHora() { return this.valorHora; }


    public void setNumHoras(int horas) { this.numHoras = horas; }

    public void setValorHora(double valor) { this.valorHora = valor; }

    public Horista(String nome, String matricula, boolean estrangeiro, int numHoras, double valorHora)
    {
        super(nome, matricula, estrangeiro);
        this.setNumHoras(numHoras);
        this.setValorHora(valorHora);
    }

}






