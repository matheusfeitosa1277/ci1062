//  1) Implemente um programa Java com as seguintes classes:
//      a. Classe lâmpada que contenha o atributo estado (ligado ou desligado) e
//      um método para ligar e desligar a lâmpada.
//      b. Classe lâmpada fluorescente que herde da classe lâmpada e contenha
//      o atributo comprimento da lâmpada em centímetros.
//      c. Classe lâmpada led que herde da classe lâmpada e não contenha
//      nenhum atributo adicional.
//      d. Classe principal que instancie um objeto para as classes lâmpada
//      fluorescente e lâmpada led. Em seguida, peça ao usuário para escolher
//      uma lâmpada e, se estiver acesa, desliga-la; caso contrário, liga-la.
//      Lembre-se de exibir o estado da lâmpada ao usuário

import java.util.Scanner;

public class S4P1
{
    public static void main(String args[])
    {
        int aux;
        Scanner input = new Scanner(System.in);
        Fluorescente lFluor = new Fluorescente();
        Led lLed = new Led();

        System.out.print("Led 1 - Fluorescente 2: ");
        aux = input.nextInt();

        //Repeticao de codigo horrorosa...
        switch(aux)
        {
            case 1:
                System.out.println(lLed.isEstado());
                lLed.alterna();
                System.out.println(lLed.isEstado());
                break;
            case 2:
                System.out.println(lFluor.isEstado());
                lFluor.alterna();
                System.out.println(lFluor.isEstado());
                break;
        }

        input.close();
    }
}

class Lampada
{
    protected boolean estado;

    public boolean isEstado() { return this.estado; }

    public void alterna()
    {
        this.estado = !this.estado;
    }
}

class Fluorescente extends Lampada
{
    private float comprimento;
}

class Led extends Lampada
{

}
