//3 - Faça um programa que leia uma palavra, calcule e exiba quantas vogais (a, e, i, o, u)
//essa palavra possui. Em seguida, entre com um caractere (consoante) e substitua todas as
//ocorrências da vogal "a‟ por esse caractere. Exiba a nova palavra na tela

import java.util.Scanner;

public class S2P3
{
    public static void main(String args[])
    {
        int i, vogais;
        char aux;
        String palavra;
        Scanner input = new Scanner(System.in);

        palavra =  input.next(); vogais = 0;
        for (i = 0; i < palavra.length(); i++)
        { 
            aux = palavra.charAt(i);
            if (aux == 'a' || aux == 'e' || aux == 'i' || aux == 'o' || aux == 'u')
                vogais ++;
        }
        System.out.printf("Vogais: %d\n", vogais);

        aux = input.next().charAt(0); /* nextChar() */
        palavra = palavra.replace('a', aux);

        System.out.println(palavra);

        input.close();
    }
}
