//2 - Faça um programa que leia uma matriz 3 x 3 formada por números inteiros. Em seguida
//gere um array unidimensional pela soma dos números de cada coluna da matriz e
//mostrar na tela esse array. Por exemplo
// 
//  Entrada:  4  8  7   Saida: 8 11 10
//           -1  2 -3
//            5  1  6
//

import java.util.Scanner;

public class S2P2
{
    private static int SIZE = 3;

    public static void main(String args[])
    {
        int i, j, aux, sum[],  matriz[][];

        Scanner input = new Scanner(System.in); 
        sum = new int[SIZE];
        matriz = new int[SIZE][SIZE];

        for (i = 0; i < matriz.length; i++) // Linhas
            for(j = 0; j < matriz[i].length; j++) // Colunas 
            {
                aux = input.nextInt();
                matriz[i][j] = aux;
                sum[j] += aux;
            }

        for (i = 0; i < sum.length; i++) System.out.printf("%d ", sum[i]);
        System.out.println();

        input.close();
    }
}
