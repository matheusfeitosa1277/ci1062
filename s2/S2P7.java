//7 - Faça um programa que leia duas frases distintas e imprima de maneira invertida,
//trocando as letras A por *

import java.util.Scanner;

public class S2P7
{
    public static void main(String args[])
    {
        int i;
        String frase;
        Scanner input = new Scanner(System.in);

        frase = input.nextLine();
        frase = frase.replace('A', '*');

        for (i = frase.length() - 1; i >= 0; i--)
            System.out.print(frase.charAt(i));

       System.out.println();

        input.close();
    }
}
