//4 - Faça um programa que leia um vetor de números reais com 7 posições. Em seguida,
//imprima o maior e o menor elemento do vetor

import java.util.Scanner;

public class S2P4
{
    private static int SIZE = 7;

    public static void main(String args[])
    {
        int i;
        float min, max, aux, vetor[];
        vetor = new float[SIZE];
        Scanner input = new Scanner(System.in);

        for (i = 0; i < SIZE; i++) vetor[i] = input.nextFloat();

        min = Integer.MAX_VALUE; max = Integer.MIN_VALUE;
        for (i = 0; i < SIZE; i++)
        {
            aux = vetor[i];
            if (aux > max) max = aux;
            if (aux < min) min = aux;
        }
        System.out.printf("Min: %.2f Max: %.2f\n", min, max);

        input.close();
    }
}
