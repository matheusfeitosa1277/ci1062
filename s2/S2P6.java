//6 - Faça um programa que leia uma matriz 4 x 4. Em seguida, leia um valor Y. O programa
//deverá buscar o valor Y na matriz e, ao final, exibir a localização (linha e coluna) ou a
//mensagem de “elemento não encontrado”.

import java.util.Scanner;

public class S2P6
{
    private static int SIZE = 4;

    public static void main(String args[])
    {
        int i, j, aux, matriz[][];
        boolean encontrado = false;
        matriz = new int[SIZE][SIZE];
        Scanner input = new Scanner(System.in);

        for (i = 0; i < matriz.length; i++)
            for (j = 0; j < matriz[i].length; j++) matriz[i][j] = input.nextInt();
        
        aux = input.nextInt();

        Loop:
        for (i = 0; i < matriz.length; i++)
            for (j = 0; j < matriz[i].length; j++) 
            {
                if (matriz[i][j] == aux)
                {
                    System.out.printf("Y encontrado em matriz[%d][%d]\n", i, j);
                    encontrado = true;
                    break Loop;
                }
            }

        if (!encontrado) System.out.println("elemento nao encontrado");

        input.close();
    }
}
