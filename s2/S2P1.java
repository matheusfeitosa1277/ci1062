//1 - Faça um programa que leia 10 números inteiros e mostre:
//
//  • Os números pares;
//  • A soma dos números pares;
//  • Os números ímpares;
//  • A quantidade de números ímpares.
//

import java.util.Scanner;


public class S2P1
{
    private static final int SIZE = 20;

    public static void main(String args[])
    {
        int nums[], aux, par, imp, i, sum;
        Scanner input = new Scanner(System.in);

        nums = new int[SIZE]; /* [10p - 10imp] */

        par = 0; imp = 0; /* Indices do Proximo elemento */
        for (i = 0; i < nums.length/2; i++) 
        {
            aux = input.nextInt();
            if ((aux & 1) == 0) /* Par */
            {
                nums[par] = aux; 
                par++;
            }
            else /* Impar */
            {
                nums[imp + nums.length/2] = aux;  
                imp++;
            }
        }

        sum = 0;
        for (i = 0; i < par; i++)
        {
            System.out.printf("%d ", nums[i]);
            sum += nums[i];
        }
        System.out.printf("\nSoma Pares: %d\n", sum);

        for (i = nums.length/2; i < imp + nums.length/2; i++)
            System.out.printf("%d ", nums[i]);

        System.out.printf("\nTotal impares: %d\n", imp);

        input.close();
   }
}
