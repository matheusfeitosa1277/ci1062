//1 - Implemente um programa com as seguintes classes Java:
//  a. Classe FuncionarioUFPR que contenha os atributos nome, matrícula, data
//  de nascimento e cargo. Crie métodos get/set para cada um dos atributos.
//  
//  b. Classe FolhaDePagamentoUFPR que contenha como atributo um
//  funcionário da UFPR, mês e ano de pagamento, e total de vencimentos
//  (que consiste no salário do mês). Crie métodos get/set para cada atributo e
//  o método imprimir que deverá exibir todos os dados da folha de
//  pagamento de um funcionário da UFPR.
//
//  c. Classe Principal que contenha o método main() para ler os dados da Folha
//  de Pagamento de três funcionários da UFPR e imprimi-los na tela.
//
//  Requisitos:
//      Crie uma classe Data para ser usada nos atributos que necessitarem
//      desse tipo
//  
//      Crie um vetor para armazenar os dados da folha de pagamento dos
//      três funcionários da UFPR.
//
//      Utilize a classe Scanner para ler os dados dos três funcionários da
//      UFPR
//

/* ------------------------------- Duvidas em Relacao aos Construtores -------------------- */


import java.util.Scanner;

public class S3P1
{
    public static int FUNC = 3; /* Numero de Funcionarios a serem criados */


    public static void main(String args[])
    {
        int i;
        Data data; FuncionarioUFPR funcionario; double salario; /* Atributos da folha */
        FolhaDePagamentoUFPR[] folha = new FolhaDePagamentoUFPR[FUNC];
        Scanner input = new Scanner(System.in);

        for (i = 0; i < FUNC; i++)
        {
            System.out.printf("Cadastrando Funcionario %d\n", i);

            data = new Data(); data.leData(input);
            funcionario = new FuncionarioUFPR(); funcionario.leFuncionario(input);
            salario = leSalario(input);

            folha[i] = new FolhaDePagamentoUFPR(funcionario, data, salario);

            System.out.printf("Funcionario %d cadastrado", i);
            System.out.println("\n");
        }

        input.close();

        for (i = 0; i < FUNC; i++)
        {
            System.out.printf("Imprimindo funcionario %d: \n", i);
            folha[i].imprimeFolha();
            System.out.println("\n");
        }

    }

    public static double leSalario(Scanner input)
    {
        System.out.printf("Digite o salario: ");

        return input.nextDouble();
    }
}

class FuncionarioUFPR
{
    private String nome, matricula, cargo;
    private Data nascimento;


    /* Getters and Setters */
    public String getNome() { return this.nome; }

    public String getMatricula(){ return this.matricula; }

    public String getCargo() { return this.cargo; }

    public Data getNascimento() { return this.nascimento; }


    public void setNome(String nome) { this.nome = nome; }

    public void setMatricula(String Matricula) { this.matricula = matricula; }

    public void setCargo(String cargo) { this.cargo = cargo; }

    public void setNascimento(Data nascimento) { this.nascimento = nascimento; }

    /* Construtores */

    /* Outros metodos */
    public void leFuncionario(Scanner input)
    {
        System.out.println("Nascimento");
        Data nascimento = new Data(); nascimento.leData(input);
        this.nascimento = nascimento;

        System.out.print("Digite nome, matricula e Cargo: ");
        input.nextLine(); // Gambi Limpar o Buffer 
        String tokens[] = input.nextLine().split(" ");

        this.nome = tokens[0];
        this.matricula= tokens[1];
        this.cargo = tokens[2];

    }
    public void imprimeFuncionario()
    {
        System.out.printf("Nome: %s, Matricula: %s, Cargo: %s\n",
            this.getNome(), this.getMatricula(), this.getCargo());

        System.out.println("Nascimento:");
        this.nascimento.imprimeData();
    }

}

class Data
{
    private int dia, mes, ano;

    /* Getters and Setters */
    public int getDia() { return this.dia; }

    public int getMes() { return this.mes; }

    public int getAno() { return this.ano; }


    public void setDia(int dia) { this.dia = dia; }

    public void setMes(int mes) { this.mes = mes; }

    public void setAno(int ano) { this.ano = ano; }

    /* Construtores */

    /* Outros metodos */
    public void leData(Scanner input)
    {
        System.out.print("Digite dia, mes e ano: ");

        this.setDia(input.nextInt());
        this.setMes(input.nextInt());
        this.setAno(input.nextInt());

    }

    public void imprimeData()
    {
       System.out.printf("Dia: %d, Mes: %d, Ano: %d\n",
                         this.getDia(), this.getMes(), this.getAno());
    }

}

class FolhaDePagamentoUFPR
{
    private FuncionarioUFPR funcionario;
    private Data dataPagamento; 
    private double salario; /* double para salario... */

    /* Getters and Setters */
    public FuncionarioUFPR getFuncionario() { return this.funcionario; }

    public Data getDataPagamento() { return this.dataPagamento; }

    public double getSalario() { return this.salario; }


    public void setFuncionario(FuncionarioUFPR funcionario) { this.funcionario = funcionario; }

    public void setDataPagamento(Data dataPagamento) { this.dataPagamento = dataPagamento; }

    public void setSalario(double salario) { this.salario= salario; }

    /* Construtores */
    public FolhaDePagamentoUFPR(FuncionarioUFPR funcionario, Data pagamento, double salario)
    {
        this.setFuncionario(funcionario);
        this.setDataPagamento(pagamento);
        this.setSalario(salario);
    }

    /* Outros metodos */
    public void imprimeFolha()
    {
        System.out.println("Imprimindo folha");
        this.dataPagamento.imprimeData();
        this.funcionario.imprimeFuncionario();
        System.out.printf("Salario: %.2f\n", this.getSalario());
    }
}

