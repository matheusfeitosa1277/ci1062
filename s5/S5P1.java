
public class S5P1
{
    public static void main(String args[])
    {

    }
}

abstract class CartaoWeb
{
    protected String destinatario;

    public abstract void retornarMensagem(String remetente);
}

class CartaoDiaDosNamorados extends CartaoWeb
{
    public void retornarMensagem(String remetente)
    {
        System.out.printf("Cartao Dia dos namorados para %s\n", remetente);
    }
}

