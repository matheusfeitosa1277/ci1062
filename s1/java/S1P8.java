//8 - Faça um programa em Java que leia os valores do peso e da altura de pessoas, enquanto
//não for digitado o número -1, conte e escreva quantas pessoas estão acima do peso. A
//condição (peso /(altura*altura)) <= 25 diz que a pessoa está no peso normal.

import java.util.Scanner;

public class S1P8
{
    public static void main(String args[])
    {
        int peso, altura, acimaP;
        Scanner input = new Scanner(System.in);

        peso = 0; altura = 1; acimaP = 0;
        while (peso != -1 && altura != -1) //Enquanto a leitura for valida
        {
            peso = input.nextInt(); altura = input.nextInt();

            if (peso/(altura*altura) > 25) acimaP++;
        }
        System.out.println(acimaP);

        input.close();
    }
}
