//3 - Faça um programa em Java que leia números inteiros enquanto não for digitado o
//    número -1, e calcule e imprima a soma destes números.

import java.util.Scanner;

public class S1P3
{
    public static void main(String args[])
    {
        int num = 0, sum = 0;
        Scanner input = new Scanner(System.in);

        while (num != -1)
        { 
            sum += num;
            num = input.nextInt();
        }

        System.out.println(sum);

        input.close();
    }
}
