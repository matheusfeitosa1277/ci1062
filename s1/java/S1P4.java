//4 - Faça um programa em Java que calcule e imprima a soma dos 10 primeiros múltiplos de 3.

public class S1P4
{
    private static int RANGE = 10;

    public static void main(String args[])
    {
        int i, sum, mul;
        sum  = 0; mul = 0;

        for (i = 0;  i < RANGE; i++)
        {
            sum += mul;
            mul += 3;
        }

        System.out.println(sum);
    }
}
