//2- Construa um programa em Java que leia um número inteiro e diga se ele é par ou ímpar.

import java.util.Scanner;

public class S1P2
{
    public static void main(String args[])
    {
        int num;

        Scanner input = new Scanner(System.in);
        num = input.nextInt();

        if ((num & 1) == 0)
            System.out.println("Par");
        else System.out.println("Impar");

        input.close();
    }
}
