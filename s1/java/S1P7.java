//7 - Faça um programa em Java que leia as 4 notas de 30 alunos da turma e escreva a maior 
//    nota de cada aluno e a maior nota da turma

import java.util.Scanner;

public class S1P7
{
    public static void main(String args[])
    {
        int i, j, aluno, turma, aux;
        Scanner input = new Scanner(System.in);

        turma = 0; //Maior Nota
        for (i = 1; i <= 30; i++)
        {
            aluno = 0;
            for (j = 0; j < 4; j++)
            {
                aux = input.nextInt();
                if (aux > aluno) aluno = aux; //Maior nota Aluno
            }
            System.out.printf("Aluno%d: %d\n", i, aluno);

            if (aluno > turma) turma = aluno;
        }
        System.out.printf("Turma: %d\n", turma);
        
        input.close();
    }
}
