//  5 - Construa um programa em Java que leia um número x, calcule e escreva o valor da
//      função f(x), dada por:
//
//      a) 0 <= x < 5, f(x) = x
//      b) 5<= x <10; f(x) = 2x + 1
//      c) x >=10; f(x) = x - 3
//

import java.util.Scanner;

public class S1P5
{
    public static void main(String args[])
    {
        int x, f;
        Scanner input = new Scanner(System.in);

        x = input.nextInt();
        
        if (x >= 0)
        {
            if (x < 5) f = x;
            else if (x < 10) f = 2*x + 1;
            else f = x - 3;

            System.out.println(f);
        }

        input.close();
    }
}
