//1 - Construa um programa em Java que leia um número e diga se ele é positivo. 

import java.util.Scanner;

public class S1P1
{
    public static void main(String args[])
    {
        float num;

        Scanner input = new Scanner(System.in);
        num = input.nextFloat();

        if (num > 0)
            System.out.println("Eh positivo");

        input.close();
    }
}
