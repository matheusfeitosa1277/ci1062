#include <iostream>
using namespace std;

int main() {
    int i, aux = -1;

    cin >> i;

    if (i > 0) {
        if (i < 5) aux = i;
        else if (i < 10) aux = 2*i + 1;
        else aux = i - 3;

        cout << aux << endl;
    }

    return 0;
}

